#ifndef LIST_LIST_H
#define LIST_LIST_H

#include <memory>
#include <experimental/optional>
#include <list>
#include <cassert>

template<typename T>
class List : public std::allocator<T> {
private:
    template<bool is_const>
    class iterator_impl;

public:
    using value_type  = typename List<T>::value_type;
    using size_type = typename List<T>::size_type;
    using pointer = typename List<T>::pointer;
    using reference  = typename List<T>::reference;
    using const_reference  = typename List<T>::const_reference;
    using iterator = iterator_impl<false>;
    using const_iterator = iterator_impl<true>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

private:
    struct Node {
        std::experimental::optional<T> data = {};
        std::shared_ptr<Node> next = nullptr;
        std::weak_ptr<Node> prev{};

        static void link(std::weak_ptr<Node> weak_a, std::shared_ptr<Node> b) {
            auto a = weak_a.lock();
            if (a) { a->next = b; }
            if (b) { b->prev = a; }
        }

        static std::shared_ptr<Node>
        insert(std::shared_ptr<Node> single, std::shared_ptr<Node> successor) {
            assert(successor != nullptr);
            assert(single != nullptr);

            auto prev_copy = successor->prev;
            link(prev_copy, single);
            link(single, successor);

            return single;
        }

        static std::shared_ptr<Node>
        erase(std::shared_ptr<Node> node) {
            link(node->prev, node->next);
            return node->next;
        }

        Node() = default;

        explicit Node(const_reference value) : data(value) {}
    };

    template<bool is_const>
    class iterator_impl {
    public:
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = List::value_type;
        using difference_type  = int;
        using pointer = std::shared_ptr<Node>;
        using reference  = std::conditional_t<is_const, value_type const &, value_type &>;
    private:
        pointer base;

        iterator_impl(std::shared_ptr<Node> node) : base(node) {}

    public:
        iterator_impl() noexcept : base(std::make_shared<Node>()) {};

        iterator_impl(iterator_impl<false> const &other) : base(other.base) {}

        template<bool X = is_const, typename SFINAE = std::enable_if_t<X, bool>>
        iterator_impl(iterator_impl<true> const &other) : base(other.base) {}

        iterator_impl &operator++() {
            base = base->next;
            return *this;
        }

        iterator_impl &operator--() {
            base = base->prev.lock();
            return *this;
        }

        iterator_impl operator++(int) {
            auto result = *this;
            ++*this;
            return result;
        }

        iterator_impl operator--(int) {
            auto result = *this;
            --*this;
            return result;
        }

        reference operator*() { return base->data.value(); }

        bool operator==(iterator_impl<true> other) const {
            return base->data == other.base->data;
        }

        bool operator==(iterator_impl<false> other) const {
            return base->data == other.base->data;
        }

        bool operator!=(iterator_impl<true> other) const {
            return not(*this == other);
        }

        bool operator!=(iterator_impl<false> other) const {
            return not(*this == other);
        }

        friend List;
    };

public:

    std::shared_ptr<Node> head = std::make_shared<Node>();
    std::shared_ptr<Node> tail = head;

public:
    List() noexcept {}

    List(std::initializer_list<T> list) : List() {
        insert(begin(), list.begin(), list.end());
    }

    reference back() { return *std::prev(end()); }

    reference front() { return *begin(); }

    const_reference back() const { return *std::prev(end()); }

    const_reference front() const { return *begin(); }

    iterator begin() { return head; }

    iterator end() { return tail; }

    const_iterator begin() const { return head; }

    const_iterator end() const { return tail; }

    reverse_iterator rbegin() { return reverse_iterator(end()); }

    reverse_iterator rend() { return reverse_iterator(begin()); }

    const_reverse_iterator rbegin() const { return const_reverse_iterator(end()); }

    const_reverse_iterator rend() const { return const_reverse_iterator(begin()); }

    bool empty() const { return head == tail; }

    void clear() {
        head = std::make_shared<Node>();
        tail = head;
    }

    iterator insert(const_iterator pos, const_reference value) {
        auto result = Node::insert(std::make_shared<Node>(value), pos.base);
        if (pos == begin()) { head = result; }
        return result;
    }

    template<typename It>
    iterator insert(const_iterator const_pos, It first, It last) {
        auto pos = const_pos;
        for (; first != last; ++first, ++pos) { pos = insert(pos, *first); }
        return iterator(pos.base);
    }

    iterator erase(const_iterator pos) {
        assert(not empty());

        auto result = Node::erase(pos.base);
        if (pos == begin()) {
            head = result;
        };
        return iterator(result);
    }

    iterator erase(const_iterator first, const_iterator last) {
        auto pos = first;
        while (pos != last) { pos = erase(pos); }
        return iterator(pos.base);
    }

    void splice(const_iterator const_pos, List &other, const_iterator first, const_iterator last_exclusive) {
        if (first == last_exclusive) {
            return;
        } else if (&other == this) {
            auto last_inclusive_base = last_exclusive.base->prev;
            Node::link(first.base->prev, last_exclusive.base);
            Node::link(const_pos.base->prev, first.base);
            Node::link(last_inclusive_base, const_pos.base);
            if (first == begin()) { head = last_exclusive.base; }
            if (const_pos == begin()) { head = first.base; }
        } else {
            auto first_prev_base = first.base->prev;
            Node::link(const_pos.base->prev, first.base);
            insert(const_pos, first, last_exclusive);
            Node::link(first_prev_base, last_exclusive.base);
            if (first == other.begin()) { other.head = last_exclusive.base; }
        }
    }

    void push_back(const_reference value) { insert(end(), value); }

    void push_front(const_reference value) { insert(begin(), value); }

    void pop_back() { erase(std::prev(end())); }

    void pop_front() { erase(begin()); }

    void swap(List &other) {
        using std::swap;
        swap(head, other.head);
        swap(tail, other.tail);
    }
};

template<typename T>
void swap(List<T> &a, List<T> &b) { a.swap(b); }


#endif //LIST_LIST_H
